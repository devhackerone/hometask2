<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters = [];

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        $maxPowerful = 0;
        $number = null;

        foreach ($this->fighters as $k => $fighter) {
            if ($fighter->getAttack() > $maxPowerful) {
                $maxPowerful = $fighter->getAttack();
                $number = $k;
            }
        }

        return $this->fighters[$number];

    }

    public function mostHealthy(): Fighter
    {
        $maxHealthy = 0;
        $number = null;

        foreach ($this->fighters as $k => $fighter) {
            if ($fighter->getHealth() > $maxHealthy) {
                $maxHealthy = $fighter->getHealth();
                $number = $k;
            }
        }

        return $this->fighters[$number];
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
