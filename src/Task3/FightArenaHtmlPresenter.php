<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $toRender = '';

        foreach($arena->all() as $person) {
            $toRender .= sprintf('<img src="%s">', $person->getImage());
            $toRender .= $person->getName()
                . ": "
                . $person->getAttack()
                . ", "
                . $person->getHealth();
            $toRender .= '<br>';
        }

        return $toRender;
    }
}
